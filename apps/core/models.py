from django.contrib.auth.base_user import AbstractBaseUser
from django.db import models

from apps.core.choices import GroupTypeChoices
from apps.core.managers import UserManager


class User(AbstractBaseUser):
    """
    User model.
    """

    USERNAME_FIELD = "email"

    REQUIRED_FIELDS = ["first_name", "last_name"]

    email = models.EmailField(verbose_name="E-mail", unique=True)

    first_name = models.CharField(verbose_name="First name", max_length=30)

    last_name = models.CharField(verbose_name="Last name", max_length=40)

    city = models.CharField(verbose_name="City", max_length=40)

    stripe_id = models.CharField(
        verbose_name="Stripe ID", unique=True, max_length=50, blank=True, null=True
    )

    objects = UserManager()

    @property
    def get_full_name(self):
        return f"{self.first_name} {self.last_name}"

    class Meta:
        verbose_name = "User"
        verbose_name_plural = "Users"


class Profile(models.Model):
    """
    User's profile.
    """

    phone_number = models.CharField(verbose_name="Phone number", max_length=15)

    date_of_birth = models.DateField(verbose_name="Date of birth")

    postal_code = models.CharField(
        verbose_name="Postal code", max_length=10, blank=True
    )

    address = models.CharField(verbose_name="Address", max_length=255, blank=True)

    class Meta:
        abstract = True


class UserProfile(Profile):
    """
    User's profile model.
    """

    user = models.OneToOneField(
        to=User,
        on_delete=models.CASCADE,
        related_name="profile",
    )

    group = models.CharField(
        verbose_name="Group type",
        choices=GroupTypeChoices.choices(),
        max_length=20,
        default=GroupTypeChoices.EMPLOYEE.name,
    )

    def __str__(self):
        return self.user.email

    class Meta:
        unique_together = ("user", "group")
