import base64
import uuid

from django.core.files.base import ContentFile


def read_image(img_path):
    """
    Reads image and returns as encoded base64 string.
    """

    with open(img_path, "rb") as f:
        return base64.b64encode(f.read())


def base64_to_internal_value(base64_data):
    """
    Base64 string into ContentFile object.
    """

    if ";base64," in base64_data:
        _, base64_data = base64_data.split(";base64,")

    name = f"{uuid.uuid4()}.png"

    return ContentFile(content=base64.b64decode(base64_data), name=name)
