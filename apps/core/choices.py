from enum import Enum


class BaseEnum(Enum):
    @classmethod
    def choices(cls):
        return [(key.name, key.value) for key in cls]


class GroupTypeChoices(BaseEnum):
    EMPLOYEE = "employee"
    EMPLOYER = "employer"
