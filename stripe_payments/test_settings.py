# -*- coding: utf-8 -*-

from .settings import *

SECRET_KEY = os.environ.get("SECRET_KEY", "fm5GnYBN2zVmLyXe")

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": ":memory:",
    }
}
