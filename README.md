Python & Stripe
===============

**Release date:** 15.09.2020

### Authors:
* Maciej Januszewski <mjanu@softserveinc.com>;

### Minimal .env file:
```bash
SECRET_KEY=xxx
DEBUG=1 # 1 or 0 | True of False
STRIPE_API_KEY=sk_test_xxx
```

### Creating environment:
```bash
virtualenv -p python3.6 env

source env/bin/activate

pip install -r requirements.txt
```

### Running:
```bash
make run_notebook
```