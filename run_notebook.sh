#!/bin/sh

set -e

python manage.py shell_plus --notebook

exec "$@"