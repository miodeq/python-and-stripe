.PHONY: clean tests

clean:
	clear; rm -rf ./.cache && \
	rm -rf db.sqlite3 && \
	rm -rf .idea && \
    rm -rf .pre-commit-venv/ && \
    find . -name ".DS_Store" -print -delete && \
    find . -name "*.pyc" -exec rm -f {} \; && \
    find . -type d -name __pycache__ -exec rm -r {} \+

black:
	clear;black --exclude="migrations" .

remove_migrations:
	clear; rm -rf **/*.pyc && \
	find . -path "*migrations*" -name "*.py" -not -path "*__init__*" -exec rm {} \;

migrations:
	clear;python manage.py makemigrations --settings=stripe_payments.test_settings

create_db: clean migrations
	clear; python manage.py migrate

run_notebook: create_db
	clear; ./run_notebook.sh